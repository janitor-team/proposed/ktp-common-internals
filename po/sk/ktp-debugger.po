# translation of ktp-debugger.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2012, 2013, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: ktp-debugger\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2015-05-15 16:01+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: debug-message-view.cpp:133
#, kde-format
msgctxt "@title:window"
msgid "Save Log"
msgstr "Uložiť záznam"

#: main-window.cpp:48
#, kde-format
msgid "Save Log"
msgstr "Uložiť záznam"

#: main-window.cpp:49
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Save log of the current tab"
msgstr "Uložiť záznam aktuálnej karty"

#: main.cpp:31
#, kde-format
msgid "KDE Telepathy Debug Tool"
msgstr "Nástroj na ladenie KDE Telepathy"

#: main.cpp:33
#, kde-format
msgid "Tool for inspecting logs of the various underlying telepathy components"
msgstr "Nástroj na inšpekciu záznamov pre rôzne základné komponenty telepathy"

#: main.cpp:35
#, kde-format
msgid "Copyright (C) 2011 Collabora Ltd."
msgstr "Copyright (C) 2011 Collabora Ltd."

#: main.cpp:36
#, kde-format
msgid "George Kiagiadakis"
msgstr "George Kiagiadakis"

#: main.cpp:36
#, kde-format
msgid "Developer"
msgstr "Vývojár"
